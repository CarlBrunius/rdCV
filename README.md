# rdCV
Repeated double cross-validation for PLS and random forest in R

## General description
This is a package for repeated double cross-validation of PLS and random forests for regression, classification and multilevel type problems. Some functions may relate to the MUVR function and create errors.  
Pls e-mail carl.brunius@chalmers.se if problems occur.

- Problem types: Regression, Classification and Multilevel
- Model cores: PLS and Random Forest
- Validation: rdCV

## Installation
Install `devtools` to be able to install packages from Git repositories.

Install `rdCV` package by:

`devtools::install_git("https://gitlab.com/CarlBrunius/rdCV.git")`

In addition to functions relevant for within/between batch correction, the package also provides workable scripts (located in your R library/rdCV/Workflow; If in doubt of the location of your library path, try `.libPaths()`) and data:  
`data("freelive")` Regression type problem of 112 observations of individuals with recorded consumption of whole grain rye and their urinary metabolome (filtered down to 1147 features). Accompanied by subject identifier to account for dependent samples.  
`data("mosquito")` Classification type problem of 29 observations of mosquitos captured from 3 villages in Burkina Faso accompanied by 16S microbiota data.  
Multilevel type problem to be added.  

## Version history
version | date | comment
:------ | :--- | :------
0.0.101 | 2018-05-02 | Fixed VIP bug (vip -> vi)
0.0.100 | 2017-10-10 | 1st commit. Ripped rdCV from MUVR package.
